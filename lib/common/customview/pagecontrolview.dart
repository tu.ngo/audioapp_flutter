import 'package:flutter/material.dart';

class PageControlView extends StatelessWidget {
  PageControlView();

  @override
  Widget build(BuildContext context) {
    return PageView(
      children: <Widget>[
        Image(
          image: AssetImage('assets/img/img1.png'),
          height: 180,
          fit: BoxFit.fitHeight,
        ),
        Image(
          image: AssetImage('assets/img/img2.png'),
          height: 180,
          fit: BoxFit.fitHeight,
        ),
        Image(
          image: AssetImage('assets/img/img3.png'),
          height: 180,
          fit: BoxFit.fitHeight,
        ),
        Image(
          image: AssetImage('assets/img/img4.png'),
          height: 180,
          fit: BoxFit.fitHeight,
        ),
      ],
    );
  }
}
