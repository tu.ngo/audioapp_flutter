import 'package:audio_app/modules/home/models/storymodel.dart';
import 'package:flutter/material.dart';
import './cells/content_listview.dart';
import './cells/categorycell.dart';
import 'package:audio_app/common/customview/pagecontrolview.dart';

class Home extends StatelessWidget {
  Home();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 20),
      child: Column(
        children: <Widget>[
          Header(),
          SizedBox(height: 10),
          Container(height: 180, child: PageControlView()),
          SizedBox(height: 10),
          Expanded(
              child: ListView(children: <Widget>[
            ContentListView(this.listStoryHot, 'Truyện nổi bật'),
            ContentListView(this.listStoryNew, 'Truyện mới nhất'),
            SizedBox(height: 10),
            Container(
              alignment: Alignment.centerLeft,
              child: Text('Thể loại truyện',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
            ),
            SizedBox(height: 10),
            CategoryCell(title: this.listTitleCategory[0]),
            SizedBox(height: 5),
            CategoryCell(title: this.listTitleCategory[1]),
            SizedBox(height: 5),
            CategoryCell(title: this.listTitleCategory[2]),
            SizedBox(height: 5),
            CategoryCell(title: this.listTitleCategory[3]),
          ])),
        ],
      ),
    ));
  }

  final List<StoryModel> listStoryHot = <StoryModel>[
    StoryModel('Thánh Gióng - Truyện cổ tích...', 'Truyện cổ tích', 1511),
    StoryModel('Truyện Kiều', 'Truyện cổ tích', 1511),
    StoryModel('Lục Vân Tiên', 'Truyện cổ tích', 1511),
  ];

  final List<StoryModel> listStoryNew = <StoryModel>[
    StoryModel('Lọ Lem', 'Truyện cổ tích', 1511),
    StoryModel('Bạch Tuyết và bảy chú lùn', 'Truyện cổ tích', 1511),
    StoryModel('Cô bé bán diêm', 'Truyện cổ tích', 1511),
  ];

  final List<String> listTitleCategory = <String>[
    'Truyện cổ tích Việt Nam',
    'Truyện cổ tích thế giới',
    'Truyện cổ tích Việt Nam',
    'Truyện cổ tích thế giới'
  ];
}

class Header extends StatelessWidget {
  Header();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(
            height: 24,
            width: 24,
            child: new IconButton(
              padding: new EdgeInsets.all(0),
              icon: Image.asset('assets/img/icon_menu.png'),
              iconSize: 24,
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        SizedBox(width: 10),
        Expanded(
          child: RaisedButton(
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                      image: AssetImage('assets/img/icon_search.png'),
                      height: 18,
                      fit: BoxFit.fitHeight),
                  SizedBox(width: 10),
                  Image(
                      image: AssetImage('assets/img/icon_find_story.png'),
                      height: 18,
                      fit: BoxFit.fitHeight),
                ],
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Colors.white),
        ),
        SizedBox(width: 10),
        SizedBox(
            height: 24,
            width: 24,
            child: new IconButton(
              padding: new EdgeInsets.all(0),
              icon: Image.asset('assets/img/icon_vip.png'),
              iconSize: 24,
              onPressed: () {},
            ))
      ],
    );
  }
}
