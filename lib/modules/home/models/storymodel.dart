class StoryModel {
  final String name;
  final String category;
  final int views;

  StoryModel(this.name, this.category, this.views);
}
