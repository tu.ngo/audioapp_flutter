import 'package:audio_app/modules/home/models/storymodel.dart';
import 'package:flutter/material.dart';

class StoryCell extends StatelessWidget {
  StoryCell({this.story, this.index});

  final int index;
  final StoryModel story;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Text('0${this.index}',
                        style: TextStyle(
                            color: Color.fromRGBO(191, 136, 119, 1),
                            fontSize: 14)),
                    Container(width: 16, height: 1, color: Colors.grey)
                  ],
                ),
              ),
              SizedBox(width: 10),
              Image(
                image: AssetImage('assets/img/icon_story_thumbnail.png'),
                width: 56,
                height: 56,
              ),
              SizedBox(width: 10),
              Expanded(
                  child: Wrap(
                direction: Axis.vertical,
                spacing: 3,
                children: <Widget>[
                  Text('${this.story.name}',
                      style: TextStyle(
                          fontSize: 14, fontWeight: FontWeight.normal)),
                  Text('Thể loại: ${this.story.category}',
                      style: TextStyle(fontSize: 10)),
                  Text('Lượt nghe: ${this.story.views}',
                      style: TextStyle(fontSize: 10))
                ],
              )),
              Container(
                  //height: double.infinity,
                  child: Image(
                image: AssetImage('assets/img/icon_crown.png'),
                width: 24,
                height: 24,
              ))
            ],
          )
        ]);
  }
}
