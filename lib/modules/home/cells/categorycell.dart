import 'package:flutter/material.dart';

class CategoryCell extends StatelessWidget {
  CategoryCell({this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color.fromRGBO(212, 241, 207, 0.34),
        child: Row(children: <Widget>[
          Container(color: Color.fromRGBO(30, 86, 42, 1), width: 1, height: 20),
          SizedBox(width: 10),
          Expanded(
              child: Text('${this.title}',
                  style: TextStyle(color: Color.fromRGBO(30, 86, 42, 1)))),
          Image(
            image: AssetImage('assets/img/icon_arrow_right.png'),
            width: 24,
            height: 24,
          )
        ]));
  }
}
