import 'package:audio_app/modules/home/models/storymodel.dart';
import 'package:flutter/material.dart';
import './storycell.dart';

class ContentListView extends StatelessWidget {
  ContentListView(this.listStory, this.title);

  final String title;
  final List<StoryModel> listStory;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('${this.title}',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
          SizedBox(height: 10),
          Container(
              height: 210,
              child: ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: listStory.length,
                itemBuilder: (BuildContext context, int index) {
                  return StoryCell(
                    story: listStory[index],
                    index: index,
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(color: Colors.grey),
              )),
          SizedBox(height: 10),
          Container(
              alignment: Alignment.center,
              child: TextButton(
                  onPressed: null,
                  child: Text(
                    'Xem thêm',
                    style: TextStyle(
                        color: Color.fromRGBO(30, 86, 42, 1), fontSize: 14),
                  )))
        ]);
  }
}
